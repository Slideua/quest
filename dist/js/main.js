$(document).ready(function($) {

  try {
    $.browserSelector();
    if($("html").hasClass("chrome")) {
      $.smoothScroll();
    }
  } catch(err) {

  };

  $('#accordion').find('.accordion-toggle').click(function(){
    var accordionToggle = $(this);
    accordionToggle.siblings().removeClass("active");
    if(accordionToggle.hasClass("active")){
      accordionToggle.removeClass("active");
    }else{
      accordionToggle.addClass("active");
    }
    //Expand or collapse this panel
    $(this).next().slideToggle();
    //Hide the other panels
    $(".accordion-content").not($(this).next()).slideUp();
  });

  new WOW().init();

  $('.counting-up').counterUp({
    delay: 2, // the delay time in ms
    time: 1000 // the speed time in ms
  });

  var pipsCount = document.getElementById('step');
  var softSlider = document.getElementById('soft');
  var softSliderValue = document.getElementById('softSliderValue');
  var range_all_sliders = {
    'min': [ 1 ],
    '20%': [ 2 ],
    '40%': [ 3 ],
    '60%': [ 4 ],
    '80%': [ 5 ],
    'max': [ 6 ]
  };

  noUiSlider.create(pipsCount, {
    range: range_all_sliders,
    start: 1,
    pips: {
      mode: 'count',
      values: 6,
      density: 50,
      stepped: true
    }
  });

  $('#step .noUi-handle').html('<input type="text" id="step_val"/>');
  pipsCount.noUiSlider.on('update', function( values, handle ){
    var step_val = document.getElementById('step');
    step_val.value = parseInt((values[handle]));
    //console.log(step_val.value);
  });

  noUiSlider.create(softSlider, {
    start: 500000,
    range: {
      min: 500000,
      max: 1500000
    },
    pips: {
      mode: 'values',
      values: [500000, 1000000, 1200000, 1500000],
      density: 10000,
      stepped: true,
      format: wNumb({
        decimals: 0,
        postfix: ' чел'
      })
    }
  });

  $('.main-slider .noUi-handle').html('<input type="text" id="softSliderValue2"/>');
  softSlider.noUiSlider.on('update', function( values, handle ){
    var softSliderValue2 = document.getElementById('softSliderValue2');
    softSliderValue2.value = parseInt((values[handle]));
    //console.log(softSliderValue2.value);
  });

  //slider_proc
  function filter500( value, type ){
    return value % 1000 ? 2 : 1;
  }

  var pipsSteps = document.getElementById('slider_proc');
  var range_all_sliders2 = {
    'min': [ 20 ],
    'max': [ 80 ]
  };

  noUiSlider.create(pipsSteps, {
    range: range_all_sliders2,
    start: 0,
    pips: {
      mode: 'steps',
      density: 3,
      filter: filter500,
      format: wNumb({
        decimals: 0,
        postfix: '%'
      })
    }
  });

  $('#slider_proc .noUi-handle').html('<div class="in-wrap"><input type="text" id="proc_val"/></div>');
  pipsSteps.noUiSlider.on('update', function( values, handle ){
    var proc_val = document.getElementById('proc_val');
    proc_val.value = parseInt((values[handle]));
    //console.log(proc_val.value);
  });

  //calc
  var legalEntity = 800,
      lumpSumPayment = 300000,
      questScenario = 100000,
      lease = 45000,
      repairs = 200000,
      ComExpenses = 650,
      offMarket = 12000,
      direct = 10000;

  var sum = legalEntity + lumpSumPayment + questScenario + lease + repairs + ComExpenses + offMarket + direct;
  console.log(sum);


  //scroll to
  $('.scrollTo').bind("click", function(e){
    var anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: $(anchor.attr('href')).offset().top
    }, 1000);
    e.preventDefault();
  });
  return false;

});


